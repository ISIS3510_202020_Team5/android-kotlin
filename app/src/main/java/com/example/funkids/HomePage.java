package com.example.funkids;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class HomePage extends AppCompatActivity {

    TextView nameText;
    TextView ageText;
    ToggleButton toggleButton1;
    ToggleButton toggleButton2;
    ToggleButton toggleButton3;
    ToggleButton toggleButton4;
    ToggleButton toggleButton5;
    ArrayList<String> preferences;
    Boolean[] checkList = new Boolean[5];
    public FirebaseAuth mAuth;
    FirebaseFirestore mFirestore;
    public String country = "";
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationRequest mLocationRequest;
    SharedPreferences prefs;
    String email;
    FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    private Object lock = new Object();
    private Object lock1 = new Object();
    NetworkInfo networkInfo;
    RecyclerView recyclerView = null;

    LocationCallback mLocationCallback = new LocationCallback();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        prefs = getApplicationContext().getSharedPreferences("prefs", MODE_PRIVATE);

        ConnectivityManager connectivityManager = (ConnectivityManager)
                getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        networkInfo = connectivityManager.getActiveNetworkInfo();

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        requestLocationUpdates();


        mFirestore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        email = mAuth.getCurrentUser().getEmail();


        nameText = findViewById(R.id.name);
        ageText = findViewById(R.id.age);
        toggleButton1 = findViewById(R.id.toggleButton1);
        toggleButton2 = findViewById(R.id.toggleButton2);
        toggleButton3 = findViewById(R.id.toggleButton3);
        toggleButton4 = findViewById(R.id.toggleButton4);
        toggleButton5 = findViewById(R.id.toggleButton5);
        getCachedData();

        preferences = new ArrayList<String>();

        threadUserDB();



        Button logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomePage.this, MainActivity.class);
                HomePage.this.finish();
                startActivity(intent);

            }
        });

        Button popular = findViewById(R.id.popular);
        popular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomePage.this, Popular.class);
                HomePage.this.finish();
                startActivity(intent);

            }
        });



        Timer timer = new Timer();
        final int MILLISECONDS = 1000; //1 seconds
        timer.schedule(new CheckConnection(this), 1000, MILLISECONDS);


    }


    private String[] getUserDB(String email) {
        final String[] info = new String[2];
        mFirestore.collection("Users").document(email).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                if (documentSnapshot.exists()) {
                    synchronized (lock) {
                        info[0] = documentSnapshot.getString("Name");
                        info[1] = String.valueOf(documentSnapshot.getLong("Age"));
                        lock.notify();
                    }
                    ArrayList<String> list = (ArrayList<String>) documentSnapshot.get("Preferences");
                    for (String i : list) {
                        preferences.add(i);
                    }
                }
            }
        });
        System.out.println("INFO: " + info[0]);
        synchronized (lock) {
            while (info[0] == null) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return info;
    }

    public void threadUserDB() {
        new AsyncTask<Void, Void, String[]>() {
            @Override
            protected String[] doInBackground(Void... params) {
                return getUserDB(email);
            }

            protected void onPostExecute(String[] info) {
                nameText.setText(info[0]);
                ageText.setText(info[1]);
            }
        }.execute();
    }

    public void threadVideoDB() {


        new AsyncTask<Void, Void, ArrayList<String>>() {
            @Override
            protected ArrayList<String> doInBackground(Void... params) {
                return getVideoDB(preferences);
            }

            protected void onPostExecute(ArrayList<String> videos) {
                Vector<YouTubeVideos> youtubeVideos = new Vector<YouTubeVideos>();

                recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                Video videoAdapter;
                for (String vid : videos) {
                    youtubeVideos.add(new YouTubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"" + vid + "\" frameborder=\"0\" allowfullscreen></iframe>"));
                    videoAdapter = new Video(youtubeVideos);

                    recyclerView.setAdapter(videoAdapter);

                }


            }
        }.execute();


    }


    private ArrayList<String> getVideoDB(ArrayList<String> preference) {

        String videosS = "Videos";
        final ArrayList<String> videos = new ArrayList<>();
        if (country.equals("Colombia")) {
            videosS = "VideosEs";
        }
        for (String pref : preference) {

            mFirestore.collection(videosS).document(pref).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {

                    if (documentSnapshot.exists()) {
                        synchronized (lock1) {
                            ArrayList<String> videosDoc = (ArrayList<String>) documentSnapshot.get("Video");
                            for (String vid : videosDoc) {
                                videos.add(vid);
                            }
                            if (videos.size() > 4) {
                                lock1.notify();
                            }
                        }
                    }
                }
            });
        }
        synchronized (lock1) {
            while (videos.isEmpty()) {
                try {
                    lock1.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return videos;
    }

    class CheckConnection extends TimerTask {
        private Context context;
        boolean check = false;
        int cont = 0;
        public CheckConnection(Context context){
            this.context = context;
        }
        public void run() {
            if(DetectConnection.isNetworkAvailable(context)){
                if(cont==0){
                    threadVideoDB();
                    cont=1;
                }
            }else if(!DetectConnection.isNetworkAvailable(context)) {
                if(!check){
                    showNoInternetDialog();
                    check=true;
                }
            }
        }
    }



    public void onClickButtons(View view) {
        if (view.getId() == R.id.toggleButton1) {
            if (toggleButton1.isChecked()) {
                checkList[0] = true;
            } else {
                checkList[0] = false;
            }
            prefs.edit().putBoolean(email + "1", checkList[0]).apply();
        }
        if (view.getId() == R.id.toggleButton2) {
            if (toggleButton2.isChecked()) {
                checkList[1] = true;
            } else {
                checkList[1] = false;
            }
            prefs.edit().putBoolean(email + "2", checkList[1]).apply();
        }
        if (view.getId() == R.id.toggleButton3) {
            if (toggleButton3.isChecked()) {
                checkList[2] = true;
            } else {
                checkList[2] = false;
            }
            prefs.edit().putBoolean(email + "3", checkList[2]).apply();
        }
        if (view.getId() == R.id.toggleButton4) {
            if (toggleButton4.isChecked()) {
                checkList[3] = true;
            } else {
                checkList[3] = false;
            }
            prefs.edit().putBoolean(email + "4", checkList[3]).apply();
        }
        if (view.getId() == R.id.toggleButton5) {
            if (toggleButton5.isChecked()) {
                checkList[4] = true;
            } else {
                checkList[4] = false;
            }
            prefs.edit().putBoolean(email + "5", checkList[4]).apply();
        }

        if (toggleButton1.isChecked() && toggleButton2.isChecked() && toggleButton3.isChecked() && toggleButton4.isChecked() && toggleButton5.isChecked()) {

            Bundle bundle = new Bundle();
            bundle.putString("Activity_completed", email);
            mFirebaseAnalytics.logEvent("Completed_activities", bundle);

            toggleButton1.setChecked(false);
            toggleButton2.setChecked(false);
            toggleButton3.setChecked(false);
            toggleButton4.setChecked(false);
            toggleButton5.setChecked(false);

            androidx.appcompat.app.AlertDialog.Builder dlgAlert = new androidx.appcompat.app.AlertDialog.Builder(HomePage.this);

            dlgAlert.setMessage("Congratulations you have completed all the activities for today!");
            dlgAlert.setTitle("Activities completed");
            dlgAlert.setPositiveButton("Ok", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

        }
    }

    public void getCachedData() {

        Bundle bundle = new Bundle();
        bundle.putString("Cached_data", email);
        mFirebaseAnalytics.logEvent("Cached_data_used", bundle);

        for (int i = 0; i < checkList.length; i++) {
            int act = i + 1;
            checkList[i] = prefs.getBoolean(email + act, false);
        }
        if (checkList[0]) {
            toggleButton1.setChecked(true);
        }
        if (checkList[1]) {
            toggleButton2.setChecked(true);
        }
        if (checkList[2]) {
            toggleButton3.setChecked(true);
        }
        if (checkList[3]) {
            toggleButton4.setChecked(true);
        }
        if (checkList[4]) {
            toggleButton5.setChecked(true);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (fusedLocationProviderClient != null) {
            requestLocationUpdates();
        }
    }

    public void requestLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(120000); // two minute interval
        mLocationRequest.setFastestInterval(120000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (fusedLocationProviderClient != null) {
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
    }


    public void showNoInternetDialog() {

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(HomePage.this);

                dlgAlert.setMessage("No Internet Connection");
                dlgAlert.setTitle("Error");
                dlgAlert.setPositiveButton("Reload", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        HomePage.super.recreate();
                    }
                });
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();
                dlgAlert.setCancelable(false);

            }

        });
    }

}