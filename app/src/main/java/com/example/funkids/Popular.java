package com.example.funkids;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Vector;

public class Popular extends AppCompatActivity {

    ArrayList<String> preferences;
    public FirebaseAuth mAuth;
    FirebaseFirestore mFirestore;
    public String country = "";
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationRequest mLocationRequest;
    SharedPreferences prefs;
    String email;
    FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    private Object lock = new Object();
    private Object lock1 = new Object();

    LocationCallback mLocationCallback = new LocationCallback();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popular);

        prefs = getApplicationContext().getSharedPreferences("prefs", MODE_PRIVATE);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);


        mFirestore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        email = mAuth.getCurrentUser().getEmail();

        preferences = new ArrayList<String>();

        threadUserDB();
        threadVideoDB();
        Bundle bundle = new Bundle();
        bundle.putString("Popular_search", email);
        mFirebaseAnalytics.logEvent("Popular_activities", bundle);


        Button logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Popular.this, MainActivity.class);
                Popular.this.finish();
                startActivity(intent);

            }
        });

        Button home = findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Popular.this, HomePage.class);
                Popular.this.finish();
                startActivity(intent);
            }
        });

    }


    private void getUserDB() {
        mFirestore.collection("Users").document("Popular").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    synchronized(lock) {
                        ArrayList<String> list = (ArrayList<String>) documentSnapshot.get("Preferences");
                        for (String i: list) {
                            String act = i;
                            preferences.add(act);
                        }
                        lock.notify();
                    }
                }
            }
        });
        synchronized(lock) {
            while (preferences.size()<2) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
    }
    }


    public void threadUserDB(){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                getUserDB();
                return null;
            }
        }.execute();
    }

    public void threadVideoDB(){
        new AsyncTask<Void, Void, ArrayList<String>>() {
            @Override
            protected ArrayList<String> doInBackground(Void... params) {
                return getVideoDB(preferences);
            }
            protected void onPostExecute(ArrayList<String> videos){
                Vector<YouTubeVideos> youtubeVideos = new Vector<YouTubeVideos>();
                RecyclerView recyclerView;
                recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                Video videoAdapter;
                for (String vid: videos) {
                    youtubeVideos.add(new YouTubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"" + vid + "\" frameborder=\"0\" allowfullscreen></iframe>"));
                    videoAdapter = new Video(youtubeVideos);
                    recyclerView.setAdapter(videoAdapter);
                }
            }
        }.execute();
    }




    private ArrayList<String> getVideoDB(ArrayList<String> preference) {

        String videosS = "Videos";
        final ArrayList<String> videos = new ArrayList<>();
        if (country.equals("Colombia")) {
            videosS = "VideosEs";
        }
        System.out.println("PREFERENCES" + preference);
        for (String pref: preference) {

            mFirestore.collection(videosS).document(pref).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {

                    if (documentSnapshot.exists()) {
                        synchronized(lock1) {
                            ArrayList<String> videosDoc = (ArrayList<String>) documentSnapshot.get("Video");
                            for (String vid: videosDoc) {
                                videos.add(vid);
                            }
                            System.out.println("VIDEOS size 123: "+ videos.size());
                            if(videos.size()>4) {
                                lock1.notify();
                            }
                        }
                    }
                }
            });
        }
        synchronized(lock1) {
            while (videos.isEmpty()) {
                try {
                    lock1.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return videos;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (fusedLocationProviderClient != null) {
            requestLocationUpdates();
        }
    }

    public void requestLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(120000); // two minute interval
        mLocationRequest.setFastestInterval(120000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (fusedLocationProviderClient != null) {
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
    }
}