package com.example.funkids;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.biometrics.BiometricPrompt;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static android.hardware.biometrics.BiometricPrompt.*;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener {


    private static final String TAG = "SignInActivity";
    public FirebaseAuth mAuth;
    EditText emailTextInput;
    EditText passwordTextInput;
    Button signInButton;
    SharedPreferences cache;
    Boolean bUser = false;


    FirebaseAnalytics mFirebaseAnalytics;
    FirebaseFirestore mFirestore;


    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ConnectivityManager connectivityManager = (ConnectivityManager)
                getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        cache = getApplicationContext().getSharedPreferences("cache", MODE_PRIVATE);

        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
        mFirestore = FirebaseFirestore.getInstance();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mAuth = FirebaseAuth.getInstance();

        emailTextInput = findViewById(R.id.editTextTextEmailAddress);
        passwordTextInput = findViewById(R.id.editTextTextPassword);
        signInButton = findViewById(R.id.login);

        loadState();

        if (networkInfo == null || !networkInfo.isConnected() || !networkInfo.isAvailable()) {
            showNoInternetDialog();
        } else {

            signInButton.setOnClickListener(view -> {

                NetworkInfo networkInfo2 = connectivityManager.getActiveNetworkInfo();
                if (networkInfo2 == null || !networkInfo2.isConnected() || !networkInfo2.isAvailable()) {

                    System.out.println("No hay internet, tratando de loggearse");
                    showNoInternetDialog();
                } else if (emailTextInput.getText().toString().contentEquals("")) {

                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(MainActivity.this);

                    dlgAlert.setMessage("Username can't be empty");
                    dlgAlert.setTitle("Error");
                    dlgAlert.setPositiveButton("Ok", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                    final AlertDialog.Builder ok = dlgAlert.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });


                } else if (passwordTextInput.getText().toString().contentEquals("")) {


                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(MainActivity.this);

                    dlgAlert.setMessage("Password can't be empty");
                    dlgAlert.setTitle("Error");
                    dlgAlert.setPositiveButton("Ok", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                    dlgAlert.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });

                } else {

                    mAuth.signInWithEmailAndPassword(emailTextInput.getText().toString(), passwordTextInput.getText().toString())
                            .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d(TAG, "signInWithEmail:success");
                                        saveState();
                                        if (!bUser) {
                                            System.out.println("Guardo los datos: " + emailTextInput.getText().toString());
                                            cache.edit().putString("email", emailTextInput.getText().toString()).apply();
                                            cache.edit().putString("pass", passwordTextInput.getText().toString()).apply();
                                            bUser = true;
                                        }
                                        FirebaseUser user = mAuth.getCurrentUser();

                                        Bundle bundle = new Bundle();
                                        bundle.putString(FirebaseAnalytics.Param.METHOD, emailTextInput.getText().toString());
                                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);

                                        if (user != null) {
                                            System.out.println("Email Verified : " + user.isEmailVerified());
                                            Intent HomeActivity = new Intent(MainActivity.this, HomePage.class);
                                            setResult(RESULT_OK, null);
                                            startActivity(HomeActivity);
                                            MainActivity.this.finish();
                                        }

                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                                        Toast.makeText(MainActivity.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();
                                        if (task.getException() != null) {
                                            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(MainActivity.this);

                                            dlgAlert.setMessage("Credentials error, please sign up first");
                                            dlgAlert.setTitle("Error");
                                            dlgAlert.setPositiveButton("Sign Up", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {


                                                    Intent intent = new Intent(MainActivity.this, Register.class);

                                                    startActivity(intent);
                                                    MainActivity.this.finish();
                                                }
                                            });
                                            dlgAlert.setNegativeButton("Ok", null);
                                            dlgAlert.setCancelable(true);
                                            dlgAlert.create().show();

                                            dlgAlert.setNegativeButton("Ok",
                                                    new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {

                                                        }
                                                    });
                                        }

                                    }

                                }
                            });


                }


            });
        }


        Executor executor = Executors.newSingleThreadExecutor();
        BiometricPrompt biometricPrompt = new Builder(this)
                .setTitle("Fingerprint Authentication")
                .setSubtitle("Subtitle")
                .setDescription("Description")
                .setNegativeButton("Cancel", executor, (dialogInterface, i) -> {
                }).build();

        Button authenticate = findViewById(R.id.button2);

        MainActivity activity = this;

        authenticate.setOnClickListener(view -> biometricPrompt.authenticate(new CancellationSignal(), executor, new AuthenticationCallback() {
            @Override
            public void onAuthenticationSucceeded(AuthenticationResult result) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "Authenticated", Toast.LENGTH_LONG).show();
                    }
                });

            }
        }));


        Executor executor2 = Executors.newSingleThreadExecutor();

        BiometricPrompt biometricPrompt2 = new Builder(this)
                .setTitle("Fingerprint")
                .setSubtitle("Sub")
                .setDescription("Desc")
                .setNegativeButton("cancel", executor, (dialogInterface, i) -> {

                }).build();

        MainActivity activity2 = this;
        authenticate.setOnClickListener(view -> {
            NetworkInfo networkInfo3 = connectivityManager.getActiveNetworkInfo();
            if (networkInfo3 == null || !networkInfo3.isConnected() || !networkInfo3.isAvailable()) {
                showNoInternetDialog();
            } else {
                String bioUser = cache.getString("email", "");
                String bioPass = cache.getString("pass", "");
                if (bioUser.equals("")) {
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(MainActivity.this);

                    dlgAlert.setMessage("No cached info for user to login");
                    dlgAlert.setTitle("Error");
                    dlgAlert.setPositiveButton("Ok", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                    dlgAlert.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                } else {
                    biometricPrompt.authenticate(new CancellationSignal(), executor, new AuthenticationCallback() {

                        @Override
                        public void onAuthenticationSucceeded(AuthenticationResult result) {

                            mAuth.signInWithEmailAndPassword(cache.getString("email", "").toString(), cache.getString("pass", "").toString())
                                    .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                // Sign in success, update UI with the signed-in user's information
                                                Log.d(TAG, "signInWithEmail:success");

                                                FirebaseUser user = mAuth.getCurrentUser();
                                                Bundle bundle = new Bundle();
                                                bundle.putString("user_email", emailTextInput.getText().toString());
                                                mFirebaseAnalytics.logEvent("FingerPrint_Login", bundle);

                                                if (user != null) {

                                                    System.out.println("Email Verified : " + user.isEmailVerified());
                                                    Intent HomeActivity = new Intent(MainActivity.this, HomePage.class);
                                                    setResult(RESULT_OK, null);
                                                    startActivity(HomeActivity);
                                                    MainActivity.this.finish();

                                                }

                                            } else {
                                                // If sign in fails, display a message to the user.
                                                Log.w(TAG, "signInWithEmail:failure", task.getException());
                                                Toast.makeText(MainActivity.this, "Authentication failed.",
                                                        Toast.LENGTH_SHORT).show();
                                                if (task.getException() != null) {
                                                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(MainActivity.this);

                                                    dlgAlert.setMessage("Failed to login with Fingerprint, check internet connection");
                                                    dlgAlert.setTitle("Error");
                                                    dlgAlert.setPositiveButton("Ok", null);

                                                    dlgAlert.setCancelable(true);
                                                    dlgAlert.create().show();

                                                    dlgAlert.setNegativeButton("Ok",
                                                            new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int i) {

                                                                }
                                                            });
                                                }

                                            }

                                        }
                                    });
                        }
                    });
                }
            }
        });


        Button register = findViewById(R.id.button3);
        register.setOnClickListener(view -> {
            Intent intent = new Intent(this, Register.class);

            startActivity(intent);
            finish();

        });


    }

    public void saveState(){
        cache.edit().putString("emailTemp", emailTextInput.getText().toString()).apply();


    }

    public void loadState(){
        String emailC = cache.getString("emailTemp", "");
        if(emailC!=""){
            emailTextInput.setText(emailC);
        }

    }

    @Override
    public void onClick(View view) {

    }

    public void showNoInternetDialog() {
        Bundle bundle = new Bundle();
        bundle.putString("lost_connection_info", "Connection lost on MainPage");
        mFirebaseAnalytics.logEvent("Connection_lost", bundle);
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.alert_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        Button btTryAgain = dialog.findViewById(R.id.bt_try_again);
        btTryAgain.setOnClickListener(view -> recreate());
        dialog.show();
    }


}