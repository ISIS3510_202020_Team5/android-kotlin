package com.example.funkids;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Register extends AppCompatActivity implements AdapterView.OnItemSelectedListener {


    private static final String TAG = "SignUpActivity";
    public FirebaseAuth mAuth;
    FusedLocationProviderClient fusedLocationProviderClient;
    Button signUpButton;
    EditText signUpEmailTextInput;
    EditText signUpPasswordTextInput;
    EditText name;
    EditText gender;
    EditText age;
    TextView locationText;
    String locationDB;
    public String country = "";
    SharedPreferences cache;

    FirebaseFirestore mFirestore;
    FirebaseAnalytics mFirebaseAnalytics;

    TextView errorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        cache = getApplicationContext().getSharedPreferences("cacheR", MODE_PRIVATE);

        ConnectivityManager connectivityManager = (ConnectivityManager)
                getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo == null || !networkInfo.isConnected() || !networkInfo.isAvailable()) {

            Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.alert_dialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
            Button btTryAgain = dialog.findViewById(R.id.bt_try_again);
            btTryAgain.setOnClickListener(view -> recreate());
            dialog.show();
        } else {
            mFirestore = FirebaseFirestore.getInstance();
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

            locationText = findViewById(R.id.location);
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);


            getLocation();

            mAuth = FirebaseAuth.getInstance();
            Spinner spinner = findViewById(R.id.dropdown_menu1);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.Sports));
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);

            Spinner spinner2 = findViewById(R.id.dropdown_menu2);
            ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.Academics));
            adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner2.setAdapter(adapter2);

            Spinner spinner3 = findViewById(R.id.dropdown_menu3);
            ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.Hobbies));
            adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner3.setAdapter(adapter3);



            Button blogin = findViewById(R.id.blogin);
            blogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    saveState();
                    Intent intent = new Intent(Register.this, MainActivity.class);
                    startActivity(intent);
                    Register.this.finish();
                }
            });

            signUpEmailTextInput = findViewById(R.id.editTextTextEmailAddress2);
            signUpPasswordTextInput = findViewById(R.id.editTextTextPassword3);
            signUpButton = findViewById(R.id.register2);
            name = findViewById(R.id.editTextTextPersonName2);
            gender = findViewById(R.id.editTextTextPersonName3);
            age = findViewById(R.id.editTextNumber);

            loadState();

            signUpButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    NetworkInfo networkInfo2 = connectivityManager.getActiveNetworkInfo();
                    if (networkInfo2 == null || !networkInfo2.isConnected() || !networkInfo2.isAvailable()) {

                        System.out.println("No hay internet, tratando de loggearse");
                        showNoInternetDialog();
                    } else if (signUpEmailTextInput.getText().toString().contentEquals("")) {

                        androidx.appcompat.app.AlertDialog.Builder dlgAlert = new androidx.appcompat.app.AlertDialog.Builder(Register.this);

                        dlgAlert.setMessage("Username can't be empty");
                        dlgAlert.setTitle("Error");
                        dlgAlert.setPositiveButton("Ok", null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();

                        final androidx.appcompat.app.AlertDialog.Builder ok = dlgAlert.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });


                    } else if (signUpPasswordTextInput.getText().toString().contentEquals("")) {


                        androidx.appcompat.app.AlertDialog.Builder dlgAlert = new androidx.appcompat.app.AlertDialog.Builder(Register.this);

                        dlgAlert.setMessage("Password can't be empty");
                        dlgAlert.setTitle("Error");
                        dlgAlert.setPositiveButton("Ok", null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();

                        dlgAlert.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });

                    } else if (name.getText().toString().contentEquals("")) {


                        androidx.appcompat.app.AlertDialog.Builder dlgAlert = new androidx.appcompat.app.AlertDialog.Builder(Register.this);

                        dlgAlert.setMessage("Enter your full name");
                        dlgAlert.setTitle("Error");
                        dlgAlert.setPositiveButton("Ok", null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();

                        dlgAlert.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });

                    } else if (age.getText().toString().contentEquals("")) {


                        androidx.appcompat.app.AlertDialog.Builder dlgAlert = new androidx.appcompat.app.AlertDialog.Builder(Register.this);

                        dlgAlert.setMessage("Enter your age");
                        dlgAlert.setTitle("Error");
                        dlgAlert.setPositiveButton("Ok", null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();

                        dlgAlert.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });

                    } else if (signUpPasswordTextInput.getText().toString().contentEquals("")) {


                        androidx.appcompat.app.AlertDialog.Builder dlgAlert = new androidx.appcompat.app.AlertDialog.Builder(Register.this);

                        dlgAlert.setMessage("Enter your gender");
                        dlgAlert.setTitle("Error");
                        dlgAlert.setPositiveButton("Ok", null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();

                        dlgAlert.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });

                    }
                    else {
                        mAuth.createUserWithEmailAndPassword(signUpEmailTextInput.getText().toString(), signUpPasswordTextInput.getText().toString())
                                .addOnCompleteListener(Register.this, new OnCompleteListener<AuthResult>() {

                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {

                                        if (task.isSuccessful()) {
                                            // Sign in success, update UI with the signed-in user's information
                                            Log.d(TAG, "createUserWithEmail:success");
                                            cache.edit().clear();
                                            List<String> prefs = new ArrayList<String>();
                                            String pref1 = spinner.getSelectedItem().toString();
                                            String pref2 = spinner2.getSelectedItem().toString();
                                            String pref3 = spinner3.getSelectedItem().toString();
                                            prefs.add(pref1);
                                            prefs.add(pref2);
                                            prefs.add(pref3);
                                            addUserDB(name.getText().toString(), signUpEmailTextInput.getText().toString(),
                                                    gender.getText().toString(), age.getText().toString(), prefs);
                                            getUserDB(signUpEmailTextInput.getText().toString());
                                            FirebaseUser user = mAuth.getCurrentUser();

                                            try {
                                                if (user != null)
                                                    user.sendEmailVerification()
                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (task.isSuccessful()) {
                                                                        Log.d(TAG, "Email sent.");

                                                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                                                                Register.this);

                                                                        // set title
                                                                        alertDialogBuilder.setTitle("Success");

                                                                        // set dialog message
                                                                        alertDialogBuilder
                                                                                .setMessage("Succesfully registered, please sign in")
                                                                                .setCancelable(false)
                                                                                .setPositiveButton("Sign In", new DialogInterface.OnClickListener() {
                                                                                    public void onClick(DialogInterface dialog, int id) {
                                                                                        Intent intent = new Intent(Register.this, MainActivity.class);
                                                                                        startActivity(intent);
                                                                                        Register.this.finish();
                                                                                    }
                                                                                });

                                                                        // create alert dialog
                                                                        AlertDialog alertDialog = alertDialogBuilder.create();

                                                                        // show it
                                                                        alertDialog.show();


                                                                    }
                                                                }
                                                            });

                                            } catch (Exception e) {
                                                errorView.setText(e.getMessage());
                                            }
                                        } else {
                                            // If sign in fails, display a message to the user.
                                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                                    Register.this);

                                            // set title
                                            alertDialogBuilder.setTitle("Error");

                                            // set dialog message
                                            alertDialogBuilder
                                                    .setMessage(task.getException().getMessage())
                                                    .setCancelable(false)
                                                    .setNegativeButton("Sign in", new DialogInterface.OnClickListener(){
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            Intent intent = new Intent(Register.this, MainActivity.class);
                                                            startActivity(intent);
                                                            Register.this.finish();
                                                        }
                                            })
                                                    .setPositiveButton("Ok", null
                                                    );


                                            // create alert dialog
                                            AlertDialog alertDialog = alertDialogBuilder.create();

                                            // show it
                                            alertDialog.show();


                                        }

                                    }
                                });

                    }

                }
            });

        }

    }

    public void saveState(){
        cache.edit().putString("email", signUpEmailTextInput.getText().toString()).apply();
        cache.edit().putString("pass", signUpPasswordTextInput.getText().toString()).apply();
        cache.edit().putString("name", name.getText().toString()).apply();
        cache.edit().putString("gender", gender.getText().toString()).apply();
        cache.edit().putString("age", age.getText().toString()).apply();
    }

    public void loadState(){
        String emailC = cache.getString("email", "");
        System.out.println("AQUI ESTA: " + emailC);
        if(emailC!=""){
            signUpEmailTextInput.setText(emailC);
        }
        String passC = cache.getString("pass", "");
        if(passC!=""){
            signUpPasswordTextInput.setText(passC);
        }
        String nameC = cache.getString("name", "");
        if(nameC!=""){
            name.setText(nameC);
        }
        String genderC = cache.getString("gender", "");
        if(genderC!=""){
            gender.setText(genderC);
        }
        String ageC = cache.getString("age", "");
        if(ageC!=""){
            age.setText(ageC);
        }

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                Location location = task.getResult();
                System.out.println("LOCATION: " + location);
                if (location != null) {
                    Geocoder geocoder = new Geocoder(Register.this, Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                        System.out.println("PAIS: " + addresses.get(0).getCountryName());
                        locationDB = addresses.get(0).getCountryName();
                        country = locationDB;
                        locationText.setText(locationDB);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }

    private void addUserDB(String name, String email, String gender, String age, List<String> prefs) {
        Map<String, Object> map = new HashMap<>();
        map.put("Name", name);
        map.put("Gender", gender);
        map.put("Email", email);
        map.put("Age", Integer.parseInt(age));
        map.put("Preferences", prefs);
        map.put("Location", locationDB);

        Bundle bundle = new Bundle();
        bundle.putString("user_preference_1", prefs.get(0));
        bundle.putString("user_preference_2", prefs.get(1));
        bundle.putString("user_preference_3", prefs.get(2));
        bundle.putString("user_name", name);
        bundle.putString("user_email", email);
        bundle.putString("gender", gender);
        bundle.putString("age", age);
        mFirebaseAnalytics.logEvent("Register", bundle);
        mFirebaseAnalytics.logEvent(prefs.get(0), bundle);
        mFirebaseAnalytics.logEvent(prefs.get(1), bundle);
        mFirebaseAnalytics.logEvent(prefs.get(2), bundle);
        mFirestore.collection("Users").document(email).set(map);

    }

    private void getUserDB(String email) {
        mFirestore.collection("Users").document(email).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    String name = documentSnapshot.getString("Name");
                    System.out.println(name);
                    String gender = documentSnapshot.getString("Gender");
                    System.out.println(gender);
                    String email = documentSnapshot.getString("Email");
                    System.out.println(email);
                    Long age = documentSnapshot.getLong("Age");
                    System.out.println(age);
                    ArrayList<String> prefs = (ArrayList<String>) documentSnapshot.get("Preferences");
                    System.out.println(prefs.get(1));
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        String text = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void showNoInternetDialog() {
        Bundle bundle = new Bundle();
        bundle.putString("lost_connection_info", "Connection lost on register");
        mFirebaseAnalytics.logEvent("Connection_lost", bundle);
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.alert_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        Button btTryAgain = dialog.findViewById(R.id.bt_try_again);
        btTryAgain.setOnClickListener(view -> recreate());
        dialog.show();
    }

}